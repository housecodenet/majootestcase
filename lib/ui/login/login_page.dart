import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:housecode/housecode.dart';
import 'package:majootestcase/ui/register/register_screen.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  Loading _loading = Loading();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) async {
          if (state is AuthBlocLoadingState) {
            _loading.show(context);
          } else {
            _loading.hide();
          }

          if(state is AuthBlocLoggedInState){
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => HomeBlocCubit()..fetching_data(),
                  child: HomeBlocScreen(),
                ),
              ),
              (route) => false,
            );
          } else if (state is AuthBlocRegisterState) {
            var value = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => RegisterBlocCubit()..openDB(),
                  child: RegisterScreen(),
                ),
              ),
            );
            context.read<AuthBlocCubit>().login();
          } else if (state is AuthBlocErrorState) {
            showMessage(context, state.error, onOk: () {
              context.read<AuthBlocCubit>().ready();
            });
          } else {
            print(state);
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Selamat Datang',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan login terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Login',
                  onPressed:()=> handleLogin(context),
                  height: 100,
                ),
                SizedBox(
                  height: 50,
                ),
                _register(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            autocorrect: false,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            autocorrect: false,
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            validator: (val) {
              return (val ?? "").length > 4 ? null : "password at least 5 characters";
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () {
          context.read<AuthBlocCubit>().register();
        },
        child: RichText(
          text: TextSpan(
              text: 'Belum punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Daftar',
                ),
              ]),
        ),
      ),
    );
  }

  void handleLogin(BuildContext context) async {
    final _email = _emailController.value;
    final _password = _passwordController.value;

    if ( _email.isNotEmpty && _password.isNotEmpty ) {
      if (!(formKey.currentState?.validate() ?? false)) {
        final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');

        if (!pattern.hasMatch(_email))
          showMessage(context, "Masukkan e-mail yang valid");
          
        return;
      }

      // AuthBlocCubit authBlocCubit = AuthBlocCubit();
      
      User user = User(
          email: _email,
          password: _password,
      );
      // authBlocCubit.login_user(user);
      context.read<AuthBlocCubit>().login_user(user);
    } else
      showMessage(context, "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
  }
}
