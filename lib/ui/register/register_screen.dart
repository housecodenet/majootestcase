import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/ui/register/register_page.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBlocCubit, RegisterBlocState>(
      builder: (context, state) {
        if (state is RegisterBlocReadyState) {
          return BlocProvider(
            create: (context) => RegisterBlocCubit()..openDB(),
            child: RegisterPage()
          );
        }

        return Center(child: Text(
          kDebugMode?"state not implemented $state": ""
        ));
      },
    );
  }
}