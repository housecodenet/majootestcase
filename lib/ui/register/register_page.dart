import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/register_bloc/register_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/models/user.dart';
import 'package:housecode/housecode.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _RegisterState();
}

class _RegisterState extends State<RegisterPage> {
  final _nameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  final _rePasswordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
      appBar: AppBar(title: Text("Register User"),),
      body: BlocListener<RegisterBlocCubit, RegisterBlocState>(
        listener: (context, state) {
          if (state is RegisterBlocErrorState) {
            showMessage(context, state.error, onOk: () {
              context.read<RegisterBlocCubit>().ready();
            });
          } else if (state is RegisterBlocUserRegisteredState) {
            showMessage(context, "Register Berhasil.", onOk: () {
              _nameController.value = "";
              _emailController.value = "";
              _passwordController.value = "";
              _rePasswordController.value = "";

              context.read<RegisterBlocCubit>().ready();
              Navigator.of(context).pop();
            },);
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 35, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Silahkan isikan data anda untuk mendaftar.',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed:()=> handleRegister(context),
                  height: 100,
                ),
              ],
            ),
          ),
        ),
      ),
    ), onWillPop: () async {
      context.read<RegisterBlocCubit>().closeDB();
      return true;
    },);
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            autocorrect: false,
            controller: _nameController,
            isEmail: true,
            hint: 'Name',
            label: 'Name',
            textInputAction: TextInputAction.next,
            validator: (val) {
              //return (val ?? "").isNotEmpty ? null : "";
            },
          ),
          CustomTextFormField(
            context: context,
            autocorrect: false,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            textInputAction: TextInputAction.next,
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            autocorrect: false,
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            textInputAction: TextInputAction.next,
            validator: (val) {
              return (val ?? "").length > 4 ? null : "password at least 5 characters";
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
          CustomTextFormField(
            context: context,
            label: 'Verifikasi Password',
            hint: 're-password',
            autocorrect: false,
            controller: _rePasswordController,
            isObscureText: _isObscurePassword,
            textInputAction: TextInputAction.done,
            validator: (val) {
              if (val != _passwordController.value) return "Password not match";
              return (val ?? "").length > 4 ? null : "password at least 5 characters";
            },
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister(BuildContext context) {
    final _name = _nameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    final _rePassword = _rePasswordController.value;

    if ( _name.isNotEmpty && _email.isNotEmpty && _password.isNotEmpty && _rePassword.isNotEmpty) {
        if (!(formKey.currentState?.validate() ?? false)) {
          final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');

          if (!pattern.hasMatch(_email))
            showMessage(context, "Masukkan e-mail yang valid");

          else if (_password != _rePassword)
            showMessage(context, "Verifikasi password tidak sesuai.");

          return;
        }

        context.read<RegisterBlocCubit>().register(User(
          email: _email,
          password: _password,
          userName: _name,
        ));
    } else {
      showMessage(context, "Form tidak boleh kosong, mohon cek kembali data yang anda inputkan");
    }
  }
}