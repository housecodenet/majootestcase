import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:percent_indicator/percent_indicator.dart';

class DetailPage extends StatelessWidget {
  final Results data;

  DetailPage({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(data.title ??
            data.originalTitle ??
            data.name ??
            data.originalName ??
            "title kosong"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Stack(
                children: [
                  Image.network("https://image.tmdb.org/t/p/w500/" +
                      (data.posterPath ?? "")),
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      margin: EdgeInsets.all(15),
                      padding:
                          EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.green.withAlpha((255 * 0.6).round())),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.star,
                            color: Colors.yellow.shade200,
                          ),
                          Container(width: 5),
                          Text(
                            "${data.voteAverage}",
                            style: TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.all(25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Expanded(
                          child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data.title ?? data.originalTitle ?? "kosong",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Container(height: 5),
                          Text(
                            (data.releaseDate ?? "").isNotEmpty
                                ? DateFormat("MMM d, yyyy").format(
                                    DateTime.parse(
                                        data.releaseDate ?? "2002-02-22"))
                                : "-",
                            style: TextStyle(color: Colors.black),
                          ),
                        ],
                      )),
                      CircularPercentIndicator(
                        radius: 80.0,
                        lineWidth: 10.0,
                        animation: true,
                        percent: data.popularity / 100 >= 10
                            ? data.popularity / 10000
                            : data.popularity / 100 > 1
                                ? data.popularity / 1000
                                : data.popularity / 100,
                        center: new Text(
                          "${data.popularity.toStringAsFixed(data.popularity.truncateToDouble() == data.popularity ? 0 : 1)}%",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 14.0),
                        ),
                        circularStrokeCap: CircularStrokeCap.round,
                        progressColor: Colors.purple,
                      ),
                    ],
                  ),
                  Container(height: 10),
                  Text(
                    "Overview",
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(height: 5),
                  Text(
                    data.overview ?? "",
                    style: TextStyle(color: Colors.black),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
