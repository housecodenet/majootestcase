import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:housecode/housecode.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  HomeBlocLoadedScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var bloc = context.read<HomeBlocCubit>();
    return Scaffold(
      appBar: AppBar(
        title: Text("The Movie DB"),
        actions: <Widget>[
          PopupMenuButton<String>(
            icon: Icon(Icons.more_vert),
            onSelected: (menu) {
              if (menu == "Logout") {
                showConfirm(context, "Apakah anda yakin mau keluar?", () {
                  context.read<HomeBlocCubit>().logout();
                  Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) =>
                            AuthBlocCubit()..fetch_history_login(),
                        child: MyHomePageScreen(),
                      ),
                    ),
                    (route) => false,
                  );
                });
              }
            },
            itemBuilder: (BuildContext context) {
              return {'Logout'}.map((choice) {
                return PopupMenuItem<String>(
                  value: choice,
                  child: Text(choice),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: SmartRefresher(
        controller: bloc.refreshController,
        enablePullDown: true,
        enablePullUp: false,
        onRefresh: () {
          bloc.fetching_data(true);
        },
        child: ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, index) {
            return movieItemWidget(data[index], context);
          },
        ),
      ),
    );
  }

  Widget movieItemWidget(Results data, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => DetailPage(
              data: data,
            ),
          ),
        );
      },
      child: Ink(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(25.0),
            ),
          ),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(25),
                child: Image.network(
                  "https://image.tmdb.org/t/p/w500/" + (data.posterPath ?? ""),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
                child: Text(
                  data.title ??
                      data.originalTitle ??
                      data.name ??
                      data.originalName ??
                      "title kosong",
                  textDirection: TextDirection.ltr,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
