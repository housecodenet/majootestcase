import 'dart:convert';

import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices{

  String _uriWithApiKey(String path) => "$path?api_key=1859a1138230ec63ed8f556ca4750814";

  Future<MovieModel?> getMovieList() async {
    try {
      var dio = await dioConfig.dio();

      var request = await dio.get<String>(_uriWithApiKey("/trending/all/day"));
      var json = jsonDecode(request.data??"{}");
      var movie = MovieModel.fromJson(json);

      return movie;
    } catch(e) {
      print(e.toString());
      return null;
    }
  }
}