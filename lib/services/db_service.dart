import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

final String tableUser = 'user_list';
final String columnId = '_id';
final String columnEmail = 'email';
final String columnPassword = 'password';
final String columnUsername = 'username';

class UserModel {
  int? id;
  String? email;
  String? password;
  String? userName;

  Map<String, Object?> toMap() {
    var map = <String, Object?>{
      columnEmail: email,
      columnPassword: password,
      columnUsername: userName ?? ""
    };
    if (id != null) {
      map[columnId] = id;
    }
    return map;
  }

  UserModel({required this.email, required this.password, this.userName, this.id});

  UserModel.fromMap(Map<String, Object?> map) {
    id = map[columnId] as int?;
    email = map[columnEmail] as String;
    password = map[columnPassword] as String;
    userName = map[columnUsername] as String?;
  }
}

class UserTableProvider {
  Database? db;

  Future open() async {
    var databasesPath = await getDatabasesPath();
    var path = join(databasesPath, "majoo.db");

    // Make sure the directory exists
    await Directory(databasesPath).create(recursive: true);
    
    db = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
      await db.execute('''
create table $tableUser ( 
  $columnId integer primary key autoincrement, 
  $columnEmail text not null,
  $columnPassword text not null,
  $columnUsername text)
''');
    });
  }

  Future<UserModel> insert(UserModel user) async {
    user.id = await db?.insert(tableUser, user.toMap());
    return user;
  }

  Future<UserModel?> getuser(int id) async {
    List<Map<String, Object?>>? maps = await db?.query(tableUser,
        columns: [columnId, columnEmail, columnPassword, columnUsername],
        where: '$columnId = ?',
        whereArgs: [id]);
    if ((maps??[]).length > 0) {
      return UserModel.fromMap(maps!.first);
    }
    return null;
  }

  Future<UserModel?> getuserByEmail(String email) async {
    List<Map<String, Object?>>? maps = await db?.query(tableUser,
        columns: [columnId, columnEmail, columnPassword, columnUsername],
        where: '$columnEmail = ?',
        whereArgs: [email]);
    if ((maps??[]).length > 0) {
      return UserModel.fromMap(maps!.first);
    }
    return null;
  }

  Future<int?> delete(int id) async {
    return await db?.delete(tableUser, where: '$columnId = ?', whereArgs: [id]);
  }

  Future<int?> update(UserModel user) async {
    return await db?.update(tableUser, user.toMap(),
        where: '$columnId = ?', whereArgs: [user.id]);
  }

  Future close() async => db?.close();
}