import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/api_service.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  final RefreshController refreshController = RefreshController();

  void fetching_data([bool fromPullRefresh = false]) async {
    if (!fromPullRefresh) emit(HomeBlocInitialState());

    try {
      var isConnected = await Connectivity().checkConnectivity();

      if (isConnected == ConnectivityResult.none ||
          isConnected == ConnectivityResult.bluetooth) {
        refreshController.refreshCompleted();
        emit(HomeBlocNetworkErrorState("offline"));
        return;
      }
    } catch (ex) {
      refreshController.refreshCompleted();
      emit(HomeBlocNetworkErrorState("offline"));
      return;
    }

    ApiServices apiServices = ApiServices();
    MovieModel? movieResponse = await apiServices.getMovieList();

    if (movieResponse == null) {
      emit(HomeBlocErrorState("Error Unknown"));
    } else {
      emit(HomeBlocLoadedState(movieResponse.results));
    }

    refreshController.refreshCompleted();
  }

  void logout() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    await sharedPreferences.setBool("is_logged_in", false);
    await sharedPreferences.setString("user_value", "");
  }
}
