import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/db_service.dart';

part 'register_bloc_state.dart';

class RegisterBlocCubit extends Cubit<RegisterBlocState> {
  RegisterBlocCubit() : super(RegisterBlocInitialState());

  UserTableProvider? _userDB;

  void openDB() async {
    try {
      _userDB = UserTableProvider();
      await _userDB?.open();
      emit(RegisterBlocReadyState());
    } catch (ex) {
      emit(RegisterBlocErrorState(ex));
    }
  }

  void ready() {
    emit(RegisterBlocReadyState());
  }

  void closeDB() {
    _userDB?.close();
  }

  void register(User user) async {
    try {
      var savedUser = await _userDB?.getuserByEmail(user.email);

      if (savedUser != null && savedUser.email == user.email) {
        emit(RegisterBlocErrorState("Email sudah terdaftar, silahkan gunakan email lain."));
        return;
      }

      var newUser = await _userDB?.insert(UserModel(email: user.email, password: user.password, userName: user.userName));
      if (newUser != null) {
        emit(RegisterBlocUserRegisteredState());
      } else {
        emit(RegisterBlocErrorState("Register user failed."));
      }
    } catch(ex) {
      emit(RegisterBlocErrorState(ex));
    }
  }
}