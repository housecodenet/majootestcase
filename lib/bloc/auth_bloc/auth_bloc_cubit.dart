import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/services/db_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async{
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool isLoggedIn = sharedPreferences.getBool("is_logged_in") ?? false;
    if(isLoggedIn){
      emit(AuthBlocLoggedInState());
    }else{
      emit(AuthBlocLoginState());
    }
  }

  void register() {
    emit(AuthBlocRegisterState());
  }

  void login() {
    emit(AuthBlocLoginState());
  }

  void ready() {
    emit(AuthBlocInitialState());
  }

  void login_user(User user) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());

    var userDB = UserTableProvider();
    await userDB.open();

    var savedUser = await userDB.getuserByEmail(user.email);

    if (savedUser == null) {
      await Future.delayed(Duration(seconds: 1), () async {
        await sharedPreferences.setBool("is_logged_in",false);
        emit(AuthBlocErrorState("User not found."));
      });
    } else {
      if (savedUser.password != user.password) {
        emit(AuthBlocErrorState("Password tidak sesuai."));
        return;
      }

      await Future.delayed(Duration(seconds: 1), () async {
        await sharedPreferences.setBool("is_logged_in",true);
        user.userName = savedUser.userName;
        String data = user.toJson().toString();
        await sharedPreferences.setString("user_value", data);
        emit(AuthBlocLoggedInState());
      });
    }

    userDB.close();
  }
}
